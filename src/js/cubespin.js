var cubeSpin = [49, 50, 51, 52]; // = 1, 2, 3, 4 keyCodes
var cubeClear = 88; // = x keyCode
var cubeSave = 83;  // = s eyCode
var cubeStop = "stop";
var cubes = [];
var active = false;

alert("Cubes are drawn centered on your current mouse location.\n\nPress 1 to draw a cube spinning on the XY axis. \nPress 2 to draw a cube spinning on the XZ axis. \nPress 3 to draw a cube spinning on the ZY axis. \nPress 4 to draw a cube spinning on all three XYZ axis.\n\nPress any key to pause.\n\nPress X to clear the trails.\n\nPress S to save the canvas as image.\n ")

function setup() {
  colorMode(HSB, 360, 100, 100, 100);
  w = window.screen.width;
  h = window.screen.height;
  createCanvas(w, h, WEBGL);
  smooth();
  background(0);
}

function draw() {
  for (var i = 0; i < cubes.length; i++) {
    cubes[i].draw();
  }
}

function keyPressed() {
  if (keyCode === cubeClear) {
    background(0);
  } else if (!active && cubeSpin.includes(keyCode)) {
    active = true;
    makeCube(keyCode);
  } else {
    active = false;
    makeCube(cubeStop);
  }

  if (keyCode === cubeSave) {
    var d = new Date();
    saveCanvas('cubespinner' + d.getTime() + '.png');
  }
}

function makeCube(angle) {
  var cube = {
    x: mouseX,
    y: mouseY,
    spin: 0,
    spinspeed: 0.02,
    spinangle: angle,
    colFade: 0,
    shapeSize: w,

    draw: function () {
      push();
      translate(this.x - (w / 2), this.y - (h / 2));
      this.colFade = (this.colFade + 1) % 360;
      stroke(this.colFade, 100, 100);
      fill(0, 0, 0, 0);
      this.spin += this.spinspeed;

      if (this.spinangle === 49) {
        rotateX(this.spin);
        rotateY(this.spin);
        box(this.shapeSize);
      } else if (this.spinangle === 50) {
        rotateX(this.spin);
        rotateZ(this.spin);
        box(this.shapeSize);
      } else if (this.spinangle === 51) {
        rotateY(this.spin);
        rotateZ(this.spin);
        box(this.shapeSize);
      } else if (this.spinangle === 52) {
        rotateZ(this.spin);
        rotateY(this.spin);
        rotateX(this.spin);
        box(this.shapeSize);
      } else if (this.spinangle === cubeStop) {
        rotateZ(0);
        rotateY(0);
        rotateX(0);
      }
      pop();
    }
  };

  if (cubes.length < 1) {
    cubes.push(cube);
  } else {
    cubes.shift();
    cubes.push(cube);
  }
}