# CubeSpin

A p5.js sketch that allows you to generate geometric desktop backgrounds.

* Press 1 to draw a cube spinning on the XY axis.
* Press 2 to draw a cube spinning on the XZ axis.
* Press 3 to draw a cube spinning on the ZY axis.
* Press 4 to draw a cube spinning on all three XYZ axis.
* Press any key to pause the cube.
* Press X to clear the trails of the spinning cube.
* Press S to save the canvas as image (it will auto size to your devices screen resolution).


Note: Cubes are drawn centered on your current mouse location. Desktop only (for now...)


Demo
https://nicolevella.com/_beta/cubespin/


